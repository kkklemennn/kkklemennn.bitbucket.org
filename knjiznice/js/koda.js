
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

function kreirajEHRzaBolnika() {
    sessionId = getSessionId();
    
    var ime = $("#kreirajIme").val();
    var priimek = $("#kreirajPriimek").val();
    var datumRojstva = $("#kreirajDatumRojstva").val();
    var visina = $("#kreirajVisino").val();
    var teza = $("#kreirajTezo").val();
    var spol = $("#kreirajSpol:checked").val();
    
    if (!ime || !priimek || !datumRojstva || !visina || !teza || !spol || ime.trim().length == 0 || priimek.trim().length == 0 ||
     datumRojstva.trim().length == 0 ||visina.trim().length == 0 || teza.trim().length == 0) {
        $("#kreirajSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite vse podatke!</span>");
        console.log("Ni vseh podatkov");
    }else {
       $.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            gender: spol,
		            partyAdditionalInfo: [{key: "height", value: visina}, {key: "weight", value: teza}, {key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "'.</span>");
		                    console.log("Uspešno kreiran EHR '" + ehrId + "'.");
		                    $("#preberiEHRid").val(ehrId);
		                    
		                    
		                    var datumInUra = "2018-05-25T07:20Z";
							var telesnaVisina = visina;
							var telesnaTeza = teza;
							var telesnaTemperatura = "36.60";
							var sistolicniKrvniTlak = "120";
							var diastolicniKrvniTlak = "80";
							var nasicenostKrviSKisikom = "100";
							var merilec = "Klemen Klemar";
						
							if (!ehrId || ehrId.trim().length == 0) {
								$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite vse podatke!!</span>");
							} else {
								$.ajaxSetup({
								    headers: {"Ehr-Session": sessionId}
								});
								var podatki = {
								    "ctx/language": "en",
								    "ctx/territory": "SI",
								    "ctx/time": datumInUra,
								    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
								    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
								   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
								    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
								    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
								    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
								    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
								};
								var parametriZahteve = { // klic template-a
								    "ehrId": ehrId,
								    templateId: 'Vital Signs', // template id
								    format: 'FLAT',
								    committer: merilec
								};
								$.ajax({
								    url: baseUrl + "/composition?" + $.param(parametriZahteve),
								    type: 'POST',
								    contentType: 'application/json',
								    data: JSON.stringify(podatki),
								    success: function (res) {
								    	console.log(res.meta.href);
								    	$("#kreirajSporocilo").html("<span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "'</span>");
								    },
								    error: function(err) {
								    	$("#kreirajSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
										console.log(JSON.parse(err.responseText).userMessage);
								    }
								});
							}
		                    
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
		            	console.log(JSON.parse(err.responseText).userMessage);
		            }
		        });
		    }
		});
    }
    
    console.log("function ran: kreirajEHRzaBolnika");
}

function preberiEHRodBolnika() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevan podatek!</span>");
	} else {
		
		var teza;
		var visina;
		
		
		var AQL = 
			
		"select " +
		"a_a/data[at0002]/events[at0003]/data[at0001]/items[at0004, 'Body weight']/value/magnitude as BWM " +
		"from EHR e[e/ehr_id/value='" + ehrId + "'] " +
		"contains OBSERVATION a_a[openEHR-EHR-OBSERVATION.body_weight.v1] " +
		"offset 0 limit 1";
				
		$.ajax({
		    url: baseUrl + "/query?" + $.param({"aql": AQL}),
		    type: 'GET',
		    headers: {"Ehr-Session": sessionId},
		    success: function (res) {
		    	teza = res.resultSet[0].BWM;
		    },
		    error: function() {
		    	alert("Napaka v poizvedbi");
		    }
		});
		 
        $.ajax({
            url: baseUrl + "/view/" + ehrId + "/height",
            type: 'GET',
            headers: {
                "Ehr-Session": sessionId
            },
            success: function (data) {
                visina = data[0].height;
                //console.log(data);
            }
        });
		
		
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
	    		console.log(data);
				var party = data.party;
				console.log(party);
				console.log(party.partyAdditionalInfo);
				var starost = 2018 - party.dateOfBirth.substring(0,4);
				var spol = party.gender;
				var bmr = (10 * teza) + (6.25 * visina) - (5 * starost);

				if (spol == "FEMALE") {
				    bmr -= 161;
				}else if (spol == "MALE") {
				    bmr += 5;
				}
				
				//$("#preberiSporocilo").html("<span class='obvestilo label label-success fade-in'>Pacient '" + spol + " " + starost + " " + party.firstNames + " " + party.lastNames + "', s tezo " + teza + " , visino " + visina +  ", BMR = " + bmr + ".</span>");
				
				$("#pacientov-bmr").html("<br>Vaš bazalni metabolizem znaša: <b>" + bmr + "</b> kalorij dnevno.")
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
				console.log(JSON.parse(err.responseText).userMessage);
			}
		});
	}	
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
    sessionId = getSessionId();
    
    var ehrId = "";
    var ime = "";
    var priimek = "";
    var datumRojstva = "";
    var visina = "";
    var teza = "";
    var spol = "";
    
    if (stPacienta == 1) {
        ime = "Nork";
        priimek = "Ckukkis";
        datumRojstva = "1940-03-10";
        visina = "178";
        teza = "70";
        spol = "MALE";
        console.log("generating patient 1");
    }else if (stPacienta == 2) {
        ime = "Tonald";
        priimek = "Drump";
        datumRojstva = "1946-06-14";
        visina = "190";
        teza = "107";
        spol = "MALE";
        console.log("generating patient 2");
    }else if (stPacienta == 3) {
        ime = "Wata";
        priimek = "Emmson";
        datumRojstva = "1990-04-15";
        visina = "165";
        teza = "51";
        spol = "FEMALE";
        console.log("generating patient 3");
    }
    
    $.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
	$.ajax({
	    url: baseUrl + "/ehr",
	    type: 'POST',
	    success: function (data) {
	        var ehrId = data.ehrId;
	        var partyData = {
	            firstNames: ime,
	            lastNames: priimek,
	            dateOfBirth: datumRojstva,
	            gender: spol,
	            partyAdditionalInfo: [{key: "height", value: visina}, {key: "weight", value: teza}, {key: "ehrId", value: ehrId}]
	        };
	        $.ajax({
	            url: baseUrl + "/demographics/party",
	            type: 'POST',
	            contentType: 'application/json',
	            data: JSON.stringify(partyData),
	            success: function (party) {
	                if (party.action == 'CREATE') {
	                    $("#generirajIDje" + stPacienta).html("<span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "'.</span>");
	                    console.log("Uspešno kreiran EHR '" + ehrId + "'.");
	                    $("#preberiEHRid").val(ehrId);
	                    
	                    
	                    var datumInUra = "2018-05-25T07:20Z";
						var telesnaVisina = visina;
						var telesnaTeza = teza;
						var telesnaTemperatura = "36.60";
						var sistolicniKrvniTlak = "120";
						var diastolicniKrvniTlak = "80";
						var nasicenostKrviSKisikom = "100";
						var merilec = "Klemen Klemar";
					
						if (!ehrId || ehrId.trim().length == 0) {
							$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite vse podatke!!</span>");
						} else {
							$.ajaxSetup({
							    headers: {"Ehr-Session": sessionId}
							});
							var podatki = {
							    "ctx/language": "en",
							    "ctx/territory": "SI",
							    "ctx/time": datumInUra,
							    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
							    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
							   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
							    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
							    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
							    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
							    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
							};
							var parametriZahteve = { // klic template-a
							    "ehrId": ehrId,
							    templateId: 'Vital Signs', // template id
							    format: 'FLAT',
							    committer: merilec
							};
							$.ajax({
							    url: baseUrl + "/composition?" + $.param(parametriZahteve),
							    type: 'POST',
							    contentType: 'application/json',
							    data: JSON.stringify(podatki),
							    success: function (res) {
							    	console.log(res.meta.href);
							    	$("#generirajIDje" + stPacienta).html("<span class='obvestilo label label-success fade-in'>Uspešno kreiran uporabnik " + ime + " " + priimek + " z EHR: '" + ehrId + "'</span>");
							    },
							    error: function(err) {
							    	$("#generirajIDje" + stPacienta).html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
									console.log(JSON.parse(err.responseText).userMessage);
							    }
							});
						}
	                    
	                }
	            },
	            error: function(err) {
	            	$("#generirajIDje" + stPacienta).html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
	            	console.log(JSON.parse(err.responseText).userMessage);
	            }
	        });
	    }
	});
        console.log("function ran: Generator");
}
$(document).ready(function() {
    $('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});
});